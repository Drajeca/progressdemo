const DetailComponent = (props) => {
    const dataItem = props.dataItem;
    return (
      <section>
        <img style={{height: '150px', width: '150px'}} src={dataItem.Picture} alt=""/>
        <p>
          <strong>Description:</strong> {dataItem.Description}
        </p>

        <p>
          <strong>Distance from center:</strong> {dataItem.DistanceFromCenter} km
        </p>
        <p style={{color: 'red'}}>
          {dataItem.AdditionalInfo}
        </p>
      </section>
    );
  };

  export default DetailComponent;