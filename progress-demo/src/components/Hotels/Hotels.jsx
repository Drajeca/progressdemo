import React, { useState } from 'react'
import { Grid, GridColumn as Column, GridToolbar } from "@progress/kendo-react-grid";
import hotels from "./hotels.json";
import DetailComponent from './DetailComponent';
import './Hotels.css'
import { orderBy } from '@progress/kendo-react-data-tools';
import { CellRender, RowRender } from "./renderers";
import { Button } from '@progress/kendo-react-buttons';
import { filterBy } from "@progress/kendo-data-query";

  const Hotels = () => {
    const [data, setData] = useState(hotels);
    const [page, setPage] = useState({
        skip: 0,
        take: 10,
      });
    const [sort, setSort] = useState([
        {
          field: "HotelName",
          dir: "asc",
        },
      ]);  
    const [editField, setEditField] = useState(undefined);
    const [changes, setChanges] = useState(false);
    const [filter, setFilter] = useState({
        logic: "and",
        filters: [
          {
            field: "HotelName",
            operator: "contains",
            value: '',
          },
        ],
      });

    const enterEdit = (dataItem, field) => {
        const newData = data.map((item) => ({
          ...item,
          inEdit: item.HotelID === dataItem.HotelID ? field : undefined,
        }));
        setData(newData);
        setEditField(field);
      };
    
      const exitEdit = () => {
        const newData = data.map((item) => ({ ...item, inEdit: undefined }));
        setData(newData);
        setEditField(undefined);
      };
    
      const saveChanges = () => {
        hotels.splice(0, hotels.length, ...data);
        setEditField(undefined);
        setChanges(true);
      };
    
      const cancelChanges = () => {
        setData(hotels);
        setChanges(false);
      };
    
      const itemChange = (event) => {
        let field = event.field || "";
        event.dataItem[field] = event.value;
        let newData = data.map((item) => {
          if (item.HotelID === event.dataItem.HotelID) {
            item[field] = event.value;
          }
    
          return item;
        });
        setData(newData);
        setChanges(true);
      };
    
      const customCellRender = (td, props) => (
        <CellRender
          originalProps={props}
          td={td}
          enterEdit={enterEdit}
          editField={editField}
        />
      );
    
      const customRowRender = (tr, props) => (
        <RowRender
          originalProps={props}
          tr={tr}
          exitEdit={exitEdit}
          editField={editField}
        />
      );

      const pageChange = (e) => {
        setPage(e.page);
      };
  
    const expandChange = (event) => {
      let newData = data.map((item) => {
        if (item.HotelID === event.dataItem.HotelID) {
          item.expanded = !event.dataItem.expanded;
        }
  
        return item;
      });
      setData(newData);
    };


  
    return (
        <div className='hotels-wallpaper'>
    <div className='hotels'>
      <Grid
        data={filterBy(orderBy(data.slice(page.skip, page.take + page.skip), sort), filter)}
        skip={page.skip}
        take={page.take}
        total={hotels.length}
        pageable={true}
        onPageChange={pageChange}
        detail={DetailComponent}
        style={{
          height: "480px",
          borderRadius: "5px"
        }}
        reorderable={true}
        resizable={true}
        expandField="expanded"
        onExpandChange={expandChange}
        sortable={true}
        sort={sort}
        onSortChange={(e) => {
          setSort(e.sort);
        }}
        rowHeight={50}
        onItemChange={itemChange}
        cellRender={customCellRender}
        rowRender={customRowRender}
        editField="inEdit"
        filterable={true}
        filter={filter}
        onFilterChange={(e) => setFilter(e.filter)}
      >
        <GridToolbar>
        <Button
          primary={true}  
          title="Save Changes"
          className="k-button"
          onClick={saveChanges}
          disabled={!changes}
        >
          Save Changes
        </Button>
        <button
          title="Cancel Changes"
          className="k-button"
          onClick={cancelChanges}
          disabled={!changes}
        >
          Cancel Changes
        </button>
      </GridToolbar>
        <Column 
            field="HotelName" 
            title="Hotel" 
            width="300px" />
        <Column 
            field="Location" 
            title="Location" 
             />
        <Column 
            field="SubwayAccess" 
            title="Subway Access" 
            width="150px"/>
         <Column 
            field="Rating" 
            title="Rating" 
            width="150px" />    

      </Grid>
    </div>
    </div>
    );
  };

export default Hotels;