import * as React from "react";
import {
  AppBar,
  AppBarSection,
  AppBarSpacer,
} from "@progress/kendo-react-layout";
import { NavLink } from "react-router-dom";
import './Header.css'

const Header = () => {
    return (
        <>
          <AppBar positionMode={'fixed'} themeColor={'dark'}>
            <AppBarSection>
              <button className="k-button k-button-clear">
                <span className="k-icon k-i-menu" />
              </button>
            </AppBarSection>
    
            <AppBarSpacer
              style={{
                width: 4,
              }}
            />
    
            <AppBarSection>
              <h1 className="title">Hotel Booking</h1>
            </AppBarSection>
    
            <AppBarSpacer
              style={{
                width: 32,
              }}
            />
    
            <AppBarSection>
              <ul className='header-list'>
                <li>
                  <NavLink exact to='/home' activeStyle={{color:'white'}}>Home</NavLink>
                </li>
                <li>
                  <NavLink exact to='/hotels' activeStyle={{color:'white'}} >Hotels</NavLink>
                </li>
                <li>
                  <NavLink exact to='/booking' activeStyle={{color:'white'}} >Booking</NavLink>
                </li>
              </ul>
            </AppBarSection>
    
            <AppBarSpacer />
            <AppBarSection>
            </AppBarSection>
          </AppBar>
        </>
      );
}

export default Header;