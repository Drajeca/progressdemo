import './Home.css'
import React from "react"
import { Button } from '@progress/kendo-react-buttons';
import { Link } from 'react-router-dom';

const Home = () => {
    return (
        <div className='homepage-wallpaper'>
            <div className='homepage-content'>
                <div>Enjoy a luxury experience</div>
                <div>and see a different side of Rome</div>
            </div>
            <Link to='/hotels' style={{textDecoration: 'none'}}><Button id='browse-button'>Browse hotels</Button></Link>
            <Link to='/booking' style={{textDecoration: 'none'}}><Button id='book-button'>Book now</Button></Link>
        </div>
    )
}

export default Home;