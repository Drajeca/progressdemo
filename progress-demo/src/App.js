import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import '@progress/kendo-theme-default/dist/all.css';
import './App.css';
import Booking from './components/Booking/Booking';
import Home from './components/Home/Home';
import Hotels from './components/Hotels/Hotels.jsx';
import Header from './components/Header/Header';

function App() {
  return (
    <BrowserRouter>
    <Header />
      <Switch>
        <Redirect exact path="/" to="/home" /> 
        <Route exact path="/home" component={Home} />
        <Route exact path="/hotels" component={Hotels} />
        <Route exact path = "/booking" component={Booking} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
